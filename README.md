lua-gettext v1.6
=
a Lua wrapper functions for Lua 5.x gettext binding

Currently hosted on [GitLab](https://gitlab.com/ports1/lua-gettext)

Feedback, pull requests and Comments welcomed
