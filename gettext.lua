-- gettext.lua
-- gettext.lua, v 1.6 2021-06-08 13:31:00
-- Lua wrapper functions for Lua 5.0.x gettext binding.
-- LICENCE: MIT, see LICENSE included for details.

-- BEGIN gettext.lua --

GetText = require("lgettext")

function _(str, ...)
	return string.format(GetText.translate(str), unpack(arg))
end

return GetText

-- END of gettext.lua --
